.PHONY : all
all: drbcc

SOURCES=parse.c lex.c main.c drbcc.h compile.c

drbcc: $(SOURCES)
	gcc -Wall -Wno-format $(SOURCES) -o drbcc

test: drbcc test.c
	cpp -P -DDRBCC test.c | ./drbcc | gcc -x assembler - -o test

.PHONY : run
run: drbcc test
	./test

.PHONY : diff
diff: test.c test
	gcc test.c -o .gcctest
	./.gcctest > .gcctest.txt | true
	./test > .test.txt | true
	diff --color=always .gcctest.txt .test.txt

.PHONY : output
output:
	cpp -DDRBCC -P test.c | ./drbcc

.PHONY : clean
clean:
	rm -f ./drbcc
	rm -f ./test
	rm -f ./.gcctest
	rm -f ./.test.txt
	rm -f ./.gcctest.txt
