#include <stdio.h>
#include <string.h>
#include <assert.h>

#include "drbcc.h"

#define BUF_SIZE 1024
static char buf[BUF_SIZE];
static char next_ch = 0;
static int i = 0;

static void consume() {
  if (i >= BUF_SIZE - 1)
    assert(0);

  buf[i++] = next_ch;
  next_ch = getchar();
  buf[i] = 0;
}

#define IS_ALPHA(c) ((c >= 'a' && c <= 'z') || (c >= 'A' && c <= 'Z'))
#define IS_DIGIT(c) (c >= '0' && c <= '9')
#define IS_WHITESPACE(c) (c == ' ' || c == '\n' || c == '\t')

int lex(char **tgt) {
  *tgt = buf;
  i = 0;
  buf[0] = 0;

  if (next_ch == 0) {
    next_ch = getchar();
    if (next_ch == EOF)
      return 0;
  }

  while (IS_WHITESPACE(next_ch))
    next_ch = getchar();

  if (next_ch == EOF || next_ch == 0)
    return 0;

  consume();

  if (IS_ALPHA(buf[0]) || buf[0] == '_') {
    while (i < BUF_SIZE - 1 && (IS_ALPHA(next_ch) || IS_DIGIT(next_ch) || next_ch == '_'))
      consume();

    /** keywords */
    if (!strcmp(buf, "int"))
      return type_tok;
    if (!strcmp(buf, "char"))
      return type_tok;
    if (!strcmp(buf, "void"))
      return type_tok;
    if (!strcmp(buf, "if"))
      return if_tok;
    if (!strcmp(buf, "else"))
      return else_tok;
    if (!strcmp(buf, "while"))
      return while_tok;
    if (!strcmp(buf, "for"))
      return for_tok;
    if (!strcmp(buf, "return"))
      return return_tok;
    if (!strcmp(buf, "break"))
      return break_tok;
    if (!strcmp(buf, "struct"))
      return struct_tok;
    if (!strcmp(buf, "union"))
      return union_tok;

    return id_tok;

  } else if (buf[0] == '|' || buf[0] == '&') {
    if (next_ch != buf[0])
      return buf[0];

    consume();

    if (!strcmp("||", buf))
      return log_or_tok;
    if (!strcmp("&&", buf))
      return log_and_tok;

    assert(0);

  } else if (IS_DIGIT(buf[0])) {
    while (IS_DIGIT(next_ch))
      consume();

    return const_tok;

  } else if (buf[0] == '\'') {
    consume();
    consume();

    if (buf[2] != '\'')
      assert(0);

    return char_lit_tok;

  } else if (buf[0] == '"') {
    while (next_ch != '"')
      consume();

    consume();

    return str_lit_tok;

  } else if (buf[0] == '+' || buf[0] == '-' || buf[0] == '=') {
    if (next_ch == buf[0]) {
      consume();

      if (!strcmp(buf, "++"))
        return inc_tok;

      if (!strcmp(buf, "--"))
        return dec_tok;

      if (!strcmp(buf, "=="))
        return eq_tok;

      assert(0);

    } else if (buf[0] == '-' && next_ch == '>') {
      consume();
      return arrow_tok;

    } else {
      return buf[0];
    }

  } else if (buf[0] == '!') {
    if (next_ch == '=') {
      consume();
      return neq_tok;
    }

    return buf[0];

  } else if (buf[0] == '<' || buf[0] == '>') {
    if (next_ch != buf[0])
      return buf[0];

    consume();

    if (!strcmp("<<", buf))
      return shl_tok;
    if (!strcmp(">>", buf))
      return shr_tok;

    assert(0);

  } else if (buf[0] == '/') {
    if (next_ch == '/') {
      while (next_ch != '\n' && next_ch != EOF)
        consume();

      return next_ch == EOF ? 0 : lex(tgt);
    }

    if (next_ch == '*') {
      consume();
      consume();

      while (next_ch != EOF) {
        if (buf[i - 1] == '*' && next_ch == '/')
          break;
        consume();
      }

      if (next_ch == EOF) return 0;

      consume();
      return lex(tgt);
    }

    return '/';
  }

  return buf[0];
}
