#include "drbcc.h"

#include <string.h>

int main() {
  struct cc_state_t *state = init_global_scope();

  while (!parse_done()) {
    struct ast_t *ast = parse_extern_decl();
    compile(ast, state);
    free_ast_mem();
  }

  cc_state_free(state);
}
