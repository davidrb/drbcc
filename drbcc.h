#ifndef DRB_CC_H_INCLUDED
#define DRB_CC_H_INCLUDED

struct ast_t;

/** functions */
int lex(char **tok_buf);
struct ast_t *parse_extern_decl();
int parse_done();

/** ast node types */
#define decl_ast 1
#define func_def_ast 2
#define decltor_ast 3
#define cmpnd_stmt_ast 4
#define empty_stmt_ast 5
#define if_stmt_ast 6
#define expr_stmt_ast 7
#define id_expr_ast 8
#define return_stmt_ast 9
#define infix_expr_ast 10
#define infix_term_ast 11
#define int_lit_ast 12
#define unary_expr_ast 13
#define id_decltor_ast 14
#define assign_expr_ast 15
#define func_param_ast 16
#define func_call_expr_ast 17
#define while_stmt_ast 18
#define tern_expr_ast 19
#define for_stmt_ast 20
#define char_lit_ast 21
#define str_lit_ast 22
#define type_spec_ast 23
#define arrow_expr_ast 25

/** ast node */
struct ast_t {
  int type;

  union {
    struct {
      struct ast_t *decltor;
      struct ast_t *body;
      struct ast_t *type;
    } as_func_def;

    struct {
      struct ast_t *decltor;
      struct ast_t *init;
      struct ast_t *type;
    } as_decl;

    struct {
      struct ast_t *direct;
      struct ast_t **params;
      int ptr_lvl;
    } as_decltor;

    struct {
      char *id;
    } as_id_decltor;

    struct {
      struct ast_t **decls;
      struct ast_t **stmts;
    } as_cmpnd_stmt;

    struct {
      struct ast_t *value;
    } as_return_stmt;

    struct {
      struct ast_t *cond;
      struct ast_t *body;
      struct ast_t *else_body;
    } as_if_stmt;

    struct {
      struct ast_t *cond;
      struct ast_t *body;
    } as_while_stmt;

    struct {
      char *id;
    } as_id_expr;

    struct {
      struct ast_t *expr;
    } as_expr_stmt;

    struct {
      struct ast_t *lhs;
      struct ast_t **terms;
    } as_infix_expr;

    struct {
      int op_tok;
      struct ast_t *value;
    } as_infix_term;

    struct {
      int value;
    } as_int_lit;

    struct {
      char value;
    } as_char_lit;

    struct {
      int *ops;
      struct ast_t *rhs;
    } as_unary_expr;

    struct {
      char *id;
      struct ast_t *lhs;
    } as_arrow_expr;

    struct {
      struct ast_t *lhs;
      struct ast_t *rhs;
    } as_assign_expr;

    struct {
      struct ast_t *type;
      struct ast_t *decltor;
    } as_func_param;

    struct {
      struct ast_t *func;
      struct ast_t **args;
    } as_func_call_expr;

    struct {
      struct ast_t *cond;
      struct ast_t *true_expr;
      struct ast_t *false_expr;
    } as_tern_expr;

    struct {
      struct ast_t *init;
      struct ast_t *cond;
      struct ast_t *update;
      struct ast_t *body;
    } as_for_stmt;

    struct {
      char *value;
    } as_str_lit;

    struct {
      char *name;
      struct ast_t **decls;
      int is_union;
    } as_type_spec;
  };
};

void print_ast(struct ast_t *ast);
struct ast_t *alloc_ast();
void free_ast_mem();

struct cc_state_t;

struct cc_state_t *init_global_scope();
void compile(struct ast_t *ast, struct cc_state_t *state);
void cc_state_free(struct cc_state_t *state);

/** token defs */
#define error_tok 0

#define id_tok 256
#define type_tok 257
#define const_tok 258
#define char_lit_tok 259
#define str_lit_tok 260
#define inc_tok 261
#define dec_tok 262
#define eq_tok 263
#define neq_tok 264
#define if_tok 265
#define while_tok 266
#define for_tok 267
#define do_tok 268
#define return_tok 269
#define break_tok 270
#define struct_tok 271
#define union_tok 272
#define else_tok 273
#define arrow_tok 274
#define log_and_tok 275
#define log_or_tok 276
#define shr_tok 277
#define shl_tok 278

#endif
