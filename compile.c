#include "drbcc.h"

#include <assert.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>

void copy_string(char **dst, char *src) {
  if (*dst) free(*dst);
  *dst = calloc(1, 1 + strlen(src));
  strcpy(*dst, src);
}

static int labeli = 0;

struct type_t {
  char *type;
  int indirection;
};

struct type_t *alloc_type() {
  return calloc(1, sizeof(struct type_t));
}

void free_type(struct type_t *t) {
  if (!t) return;

  if (t->type) free(t->type);
  free(t);
}

struct struct_def_t {
  int num_fields;
  char *fields[20];
  long offsets[20];
  struct type_t types[20];
};

struct cc_state_t {
  /** used registers */
  long regs;

  /** scope */
  int ret_lbl;
  int stack_sz;
  int next_rbp;
  int stack_align;

  /** string literals */
  int stri;
  char *str_lits[100];
  int str_def_i;

  /** symbol table */
  int sym_i;
  char *sym_names[100];
  char *sym_locs[100];
  struct type_t sym_types[100];

  /** structs and unions */
  int num_structs;
  char *struct_ids[100];
  struct struct_def_t struct_defs[100];

  /** parent scope */
  struct cc_state_t *prev;

  struct type_t *ret_type;
};

struct cc_state_t *init_global_scope() {
  struct cc_state_t *state = calloc(1, sizeof(struct cc_state_t));
  memset(state, 0, sizeof(struct cc_state_t));
  return state;
}

static struct cc_state_t *init_subscope(struct cc_state_t *state) {
  struct cc_state_t *sc = init_global_scope();
  sc->ret_lbl = state->ret_lbl;
  sc->next_rbp = state->next_rbp;
  sc->regs = state->regs;
  sc->stack_sz = 0;
  sc->prev = state;
  sc->sym_i = 0;
  sc->stri = 0;
  sc->num_structs = 0;
  return sc;
}

static char *get_decltor_id(struct ast_t *ast) {
  struct ast_t *direct = 0;
  assert(ast);
  assert(ast->type == decltor_ast);

  direct = ast->as_decltor.direct;

  if (direct->type == id_decltor_ast)
    return direct->as_id_decltor.id;

  return get_decltor_id(direct);
}

static int get_decltor_indirection(struct ast_t *ast) {
  if (ast->type == decltor_ast) {
    int i = ast->as_decltor.ptr_lvl;
    // if (ast->as_decltor.params)
    //   ++i;

    return i + get_decltor_indirection(ast->as_decltor.direct);

  } else if (ast->type == id_decltor_ast) {
    return 0;

  } else {
    fprintf(stderr, "%i\n", ast->type);
    assert(0);
  }
}

static int get_ret_pop_sz(struct cc_state_t *state) {
  if (state->prev)
    return state->stack_sz + get_ret_pop_sz(state->prev);

  return state->stack_sz;
}

static struct type_t *get_ret_type(struct cc_state_t *state) {
  if (state->ret_type)
    return state->ret_type;

  if (state->prev)
    return get_ret_type(state->prev);

  assert(0);
}

static void define_symbol(char *sym, char *loc, struct type_t *t, struct cc_state_t *state) {
  if (state->sym_i + 1 >= 100) {
    fprintf(stderr, "symbol table overflow\n");
    assert(0);
  }

  copy_string(&state->sym_names[state->sym_i], sym);
  copy_string(&state->sym_locs[state->sym_i], loc);

  copy_string(&state->sym_types[state->sym_i].type, t->type);
  state->sym_types[state->sym_i].indirection = t->indirection;

  ++state->sym_i;
}

static struct struct_def_t *get_struct_def(char *id, struct cc_state_t *state) {
  int i;
  for (i = 0; i < state->num_structs; ++i) {
    if (!strcmp(id, state->struct_ids[i])) {
      return &state->struct_defs[i];
    }
  }

  if (state->prev)
    return get_struct_def(id, state->prev);

  fprintf(stderr, "no struct def %s\n", id);
  assert(0);
}

static int sizeof_type(struct type_t *t, struct cc_state_t *state) {
  int i;
  int sz = 0;
  struct struct_def_t *sd = 0;

  if (t->indirection)
    return 8;

  if (!strcmp(t->type, "char"))
    return 1;

  if (!strcmp(t->type, "int"))
    return 4;

  sd = get_struct_def(t->type, state);
  for (i = 0; i < sd->num_fields; ++i) {
    int s = sd->offsets[i] + sizeof_type(&sd->types[i], state);
    sz = s > sz ? s : sz;
  }

  return sz;
}

static void define_struct(struct ast_t *ast, struct cc_state_t *state) {
  int di;
  long off = 0;

  if (ast->as_type_spec.decls) {
    int si = 0;
    struct struct_def_t *sd = 0;

    for (di = 0; ast->as_type_spec.decls[di]; ++di)
      define_struct(ast->as_type_spec.decls[di]->as_decl.type, state);

    si = state->num_structs++;
    sd = &state->struct_defs[si];

    copy_string(&state->struct_ids[si], ast->as_type_spec.name);

    for (di = 0; ast->as_type_spec.decls[di]; ++di) {
      struct ast_t *decl = 0;
      assert(state->num_structs < 100);
      decl = ast->as_type_spec.decls[di];

      copy_string(&sd->fields[di], get_decltor_id(decl->as_decl.decltor));

      copy_string(&sd->types[di].type, decl->as_decl.type->as_type_spec.name);
      sd->types[di].indirection = get_decltor_indirection(decl->as_decl.decltor);

      sd->offsets[di] = ast->as_type_spec.is_union ? 0 : off;

      off += sizeof_type(&sd->types[di], state);
      if (off%8)
        off += 8 - off%8;
    }

    sd->num_fields = di;
  }
}

static int add_str_lit(char *str, struct cc_state_t *state) {
  assert(state->stri < 100);

  while (state->prev)
    state = state->prev;

  copy_string(&state->str_lits[state->stri], str);

  return state->stri++;
}

static char *offset_reg(int offset, char *reg) {
  int sz = snprintf(0, 0, "%i(%s)", offset, reg);
  char *r = calloc(1, sz + 1);
  snprintf(r, sz + 1, "%i(%s)", offset, reg);
  return r;
}

static char *get_sym_loc(char *sym, struct cc_state_t *state) {
  int i;
  assert(sym);

  for (i = 0; i < state->sym_i; ++i) {
    if (!strcmp(sym, state->sym_names[i]))
      return state->sym_locs[i];
  }

  if (state->prev)
    return get_sym_loc(sym, state->prev);

  fprintf(stderr, "symbol %s not found\n", sym);
  assert(0);
}

static struct type_t *get_sym_type(char *sym, struct cc_state_t *state) {
  int i;
  assert(sym);

  for (i = 0; i < state->sym_i; ++i) {
    if (!strcmp(sym, state->sym_names[i]))
      return &state->sym_types[i];
  }

  if (state->prev)
    return get_sym_type(sym, state->prev);

  fprintf(stderr, "symbol %s not found\n", sym);
  assert(0);
}

static int get_struct_field(char *id, char *field, struct cc_state_t *state) {
  struct struct_def_t *sd = get_struct_def(id, state);
  int j;
  for (j = 0; j < sd->num_fields; ++j) {
    if (!strcmp(field, sd->fields[j])) {
      return j;
    }
  }
  fprintf(stderr, "no field %s\n", field);
  assert(0);
}

#define ALLOC_REG(i, reg) \
  if ((state->regs & i) == 0) { \
    state->regs = state->regs | i; \
    return reg; \
  }

char *alloc_reg(struct cc_state_t *state) {
  ALLOC_REG(1, "%r8");
  ALLOC_REG(2, "%r9");
  ALLOC_REG(4, "%r10");
  ALLOC_REG(8, "%r11");
  ALLOC_REG(16, "%r12");
  ALLOC_REG(32, "%r13");
  ALLOC_REG(64, "%r14");
//  ALLOC_REG(128, "%r15");

  fprintf(stderr, "out of registers\n");
  assert(0);
}

char *deref_reg(char *reg) {
  if (!strcmp("%r8", reg)) return "(%r8)";
  if (!strcmp("%r9", reg)) return "(%r9)";
  if (!strcmp("%r10", reg)) return "(%r10)";
  if (!strcmp("%r11", reg)) return "(%r11)";
  if (!strcmp("%r12", reg)) return "(%r12)";
  if (!strcmp("%r13", reg)) return "(%r13)";
  if (!strcmp("%r14", reg)) return "(%r14)";
//  if (!strcmp("%r15", reg)) return "(%r15)";
  assert(0);
}

#define FREE_REG(i, r) \
  if (!strcmp(r, reg) || !strcmp(reg, deref_reg(r))) { \
    state->regs = state->regs & ~i; \
    return; \
  }

void free_reg(char *reg, struct cc_state_t *state) {
  FREE_REG(1, "%r8");
  FREE_REG(2, "%r9");
  FREE_REG(4, "%r10");
  FREE_REG(8, "%r11");
  FREE_REG(16, "%r12");
  FREE_REG(32, "%r13");
  FREE_REG(64, "%r14");
//  FREE_REG(128, "%r15");
}

static int is_reg(char *reg) {
  if (!strcmp(reg, "%r8")) return 1;
  if (!strcmp(reg, "%r9")) return 1;
  if (!strcmp(reg, "%r10")) return 1;
  if (!strcmp(reg, "%r11")) return 1;
  if (!strcmp(reg, "%r12")) return 1;
  if (!strcmp(reg, "%r13")) return 1;
  if (!strcmp(reg, "%r14")) return 1;
  if (!strcmp(reg, "%r15")) return 1;
  return 0;
}

static char *ensure_reg(char *loc, struct cc_state_t *state, struct type_t *t) {
  char *suf = "";

  if (!is_reg(loc)) {
    if (sizeof_type(t, state) == 1)
      suf = "b";
    if (sizeof_type(t, state) == 4)
      suf = "w";

    char *reg = alloc_reg(state);
    printf("xor %s, %s\n", reg, reg);
    printf("mov %s, %s%s\n", loc, reg, suf);
    free_reg(loc, state);
    return reg;
  }

  return loc;
}

static int sizeof_target_type(struct type_t *t, struct cc_state_t *state) {
  int sz;
  if (t->indirection > 1)
    return 8;

  --t->indirection;
  sz = sizeof_type(t, state);
  ++t->indirection;
  return sz;
}

#define IS_PRIMITIVE(t) (!strcmp(t->type, "void") || !strcmp(t->type, "char") || !strcmp(t->type, "int"))

void clamp_reg_to_type(char *reg, struct type_t *t, struct cc_state_t *state) {
  if (t->indirection == 0) {
    char *r = alloc_reg(state);

    if (!strcmp("char", t->type)) {
      printf("mov $0xFF, %s\n", r);
      printf("and %s, %s\n", r, reg);

    } else if (!strcmp("int", t->type)) {
      printf("mov $0xFFFF, %s\n", r);
      printf("and %s, %s\n", r, reg);
    }

    free_reg(r, state);
  }
}

char *compile_expr(struct ast_t *ast, struct cc_state_t *state, struct type_t *t);

char *compile_expr_(struct ast_t *ast, struct cc_state_t *state, struct type_t *t) {
  assert(ast);

  /** integer literal */
  if (ast->type == int_lit_ast) {
    char *reg = alloc_reg(state);
    printf("mov $%i, %s\n", ast->as_int_lit.value, reg);

    copy_string(&t->type, "int");
    t->indirection = 0;

    return reg;

  } else if (ast->type == str_lit_ast) {
    char *reg = alloc_reg(state);
    int i = add_str_lit(ast->as_str_lit.value, state);
    printf("leaq STR%i(%rip), %s\n", i, reg);

    copy_string(&t->type, "char");
    t->indirection = 1;

    return reg;

  } else if (ast->type == char_lit_ast) {
    char *reg = alloc_reg(state);
    printf("xor %s, %s\n", reg, reg);
    printf("movb $%i, %sb\n", (int)ast->as_char_lit.value, reg);

    copy_string(&t->type, "char");
    t->indirection = 0;

    return reg;

  } else if (ast->type == arrow_expr_ast) {
    int i;
    int off;
    struct struct_def_t *sd = 0;
    char *reg = compile_expr(ast->as_arrow_expr.lhs, state, t);
    reg = ensure_reg(reg, state, t);

    sd = get_struct_def(t->type, state);
    i = get_struct_field(t->type, ast->as_arrow_expr.id, state);
    off = sd->offsets[i];

    printf("add $%i, %s\n", off, reg);

    copy_string(&t->type, sd->types[i].type);
    t->indirection = sd->types[i].indirection;

    if (IS_PRIMITIVE(t) || (t->indirection > 1))
      reg = deref_reg(reg);

    return reg;

  /** unary expressions */
  } else if (ast->type == unary_expr_ast) {
    int i;
    int num_ops;
    char *rhs = compile_expr(ast->as_unary_expr.rhs, state, t);
    //rhs = ensure_reg(rhs, state);

    for (num_ops = 0; ast->as_unary_expr.ops[num_ops]; ++num_ops);
    for (i = num_ops; i > 0; --i) {
      int op = ast->as_unary_expr.ops[i - 1];

      /** deref pointer */
      if (op == '*') {
        assert(t->indirection);

        if (IS_PRIMITIVE(t) || t->indirection > 1) {
          rhs = ensure_reg(rhs, state, t);
          rhs = deref_reg(rhs);
          --t->indirection;
        }

      /** addr of lvalue */
      } else if (op == '&') {
        if (IS_PRIMITIVE(t) || t->indirection > 1) {
          assert(!is_reg(rhs));
          ++t->indirection;

          char *reg = alloc_reg(state);
          printf("lea %s, %s\n", rhs, reg);
          free_reg(rhs, state);
          rhs = reg;

        } else {
          // struct object, do nothing
        }

      } else if (op == '!') {
        rhs = ensure_reg(rhs, state, t);
        printf("xor %rax, %rax\n");
        printf("cmp $0, %s\n", rhs);
        printf("sete %%al\n");
        printf("mov %rax, %s\n", rhs);

        copy_string(&t->type, "int");
        t->indirection = 0;

      } else if (op == inc_tok || op == dec_tok) {
        char *inst = op == inc_tok ? "inc" : "dec";

        if (sizeof_type(t, state) == 1)
          printf("%sb %s\n", inst, rhs);
        else if (sizeof_type(t, state) == 4)
          printf("%sw %s\n", inst, rhs);
        else if (sizeof_type(t, state) == 8)
          printf("%sq %s\n", inst, rhs);

      } else {
        assert(0);
      }

      if (is_reg(rhs))
        clamp_reg_to_type(rhs, t, state);
    }

    return rhs;

  /** ternary expression */
  } else if (ast->type == tern_expr_ast) {
    char *reg;
    char *r;
    char *cond = compile_expr(ast->as_tern_expr.cond, state, t);
    int else_lbl = ++labeli;
    int end_lbl = ++labeli;

    printf("cmp $0, %s\n", cond);
    free_reg(cond, state);

    reg = alloc_reg(state);

    printf("je LABEL%i\n", else_lbl);
    r = compile_expr(ast->as_tern_expr.true_expr, state, t);
    printf("mov %s, %s\n", r, reg);
    printf("jmp LABEL%i\n", end_lbl);
    free_reg(r, state);

    printf("LABEL%i:\n", else_lbl);
    r = compile_expr(ast->as_tern_expr.false_expr, state, t);
    printf("mov %s, %s\n", r, reg);
    free_reg(r, state);

    printf("LABEL%i:\n", end_lbl);
    return reg;

  /** infix expressions */
  } else if (ast->type == infix_expr_ast) {
    int i;
    char *lhs = compile_expr(ast->as_infix_expr.lhs, state, t);
    lhs = ensure_reg(lhs, state, t);
    int short_circuit_lbl = 0;

    for (i = 0; ast->as_infix_expr.terms[i]; ++i) {
      struct type_t *t2 = 0;
      struct ast_t *term = ast->as_infix_expr.terms[i];
      int op = ast->as_infix_expr.terms[i]->as_infix_term.op_tok;
      int sz = 1;
      char *next;

      /** short circuit logical expressions */
      if (op == log_or_tok || op == log_and_tok) {
        if (!short_circuit_lbl)
          short_circuit_lbl = ++labeli;

        printf("cmp $0, %s\n", lhs);

        if (op == log_or_tok)
          printf("jnz LABEL%i\n", short_circuit_lbl);
        else if (op == log_and_tok)
          printf("jz LABEL%i\n", short_circuit_lbl);
      }

      /** evaluate next operand */
      t2 = alloc_type();
      next = compile_expr(term->as_infix_term.value, state, t2);
      next = ensure_reg(next, state, t2);

      /** do pointer type coercion */
      if (t->indirection && t2->indirection == 0 && op == '+') {
        sz = sizeof_target_type(t, state);
        printf("imul $%i, %s\n", sz, next);

      } else if (t2->indirection && t->indirection == 0 && op == '+') {
        sz = sizeof_target_type(t2, state);
        printf("imul $%i, %s\n", sz, lhs);

        t->indirection = t2->indirection;
        copy_string(&t->type, t2->type);

      } else if (t->indirection && t2->indirection && op == '+') {
        assert(0);
      }

      if (op == '+' || op == '-') {
        char *inst = op == '+' ? "add" : "sub";
        printf("%s %s, %s\n", inst, next, lhs);
        free_reg(next, state);

      } else if (op == '*') {
        printf("imul %s, %s\n", next, lhs);

      } else if (op == '/' || op == '%') {
        printf("mov %s, %rax\n", lhs);
        printf("cqo\n");
        printf("idiv %s\n", next);

        if (op == '/')
          printf("mov %rax, %s\n", lhs);
        else if (op == '%')
          printf("mov %rdx, %s\n", lhs);

        free_reg(next, state);

      // TODO: add other operators
      } else if (op == eq_tok || op == neq_tok || op == '<' || op == '>') {
        printf("xor %rax, %rax\n");
        printf("cmp %s, %s\n", next, lhs);

        if (op == eq_tok)
          printf("sete %%al\n");
        else if (op == neq_tok)
          printf("setne %%al\n");
        else if (op == '<')
          printf("setl %%al\n");
        else if (op == '>')
          printf("setg %%al\n");

        printf("mov %rax, %s\n", lhs);
        free_reg(next, state);

        copy_string(&t->type, "int");
        t->indirection = 0;

      } else if (op == log_or_tok) {
        printf("or %s, %s\n", next, lhs);
        free_reg(next, state);

      } else if (op == log_and_tok) {
        printf("and %s, %s\n", next, lhs);
        free_reg(next, state);

      } else {
        printf("# unimplemented\n");
        free_reg(next, state);
      }

      free_type(t2);
    }

    if (short_circuit_lbl) {
      printf("LABEL%i:\n", short_circuit_lbl);
    }

    return lhs;

  /** identifier expression */
  } else if (ast->type == id_expr_ast) {
    char *loc = get_sym_loc(ast->as_id_expr.id, state);
    struct type_t *t2 = get_sym_type(ast->as_id_expr.id, state);
    copy_string(&t->type, t2->type);
    t->indirection = t2->indirection;

    if (!IS_PRIMITIVE(t) && t->indirection == 0) {
      char *reg = alloc_reg(state);
      printf("leaq %s, %s\n", loc, reg);
      free_reg(loc, state);
      loc = reg;
    }

    return loc;

  /** assignment expression */
  } else if (ast->type == assign_expr_ast) {
    struct type_t *t2 = alloc_type();
    char *suf = "";

    char *dst = compile_expr(ast->as_assign_expr.lhs, state, t);
    char *src = compile_expr(ast->as_assign_expr.rhs, state, t2);

    if (is_reg(dst)) {
      printf("xor %s, %s\n", dst, dst);

    } else {
      if (sizeof_type(t, state) == 1)
        suf = "b";
      if (sizeof_type(t, state) == 4)
        suf = "w";
    }

    src = ensure_reg(src, state, t2);

    clamp_reg_to_type(src, t, state);

    if (is_reg(dst))
      printf("xor %s, %s\n", dst, dst);

    printf("mov %s%s, %s\n", src, suf, dst);

    free_reg(dst, state);
    free_type(t2);

    return src;

  /** function calls */
  } else if (ast->type == func_call_expr_ast) {
    int num_args;
    int i;
    char *ret = 0;
    char *func = compile_expr(ast->as_func_call_expr.func, state, t);
    //--t->indirection;

    for (num_args = 0; ast->as_func_call_expr.args[num_args]; ++num_args);
    assert(num_args <= 4);

    for (i = 0; i < num_args; ++i) {
      struct type_t *t2 = alloc_type();

      char *reg = compile_expr(ast->as_func_call_expr.args[i], state, t2);

      reg = ensure_reg(reg, state, t2);
      clamp_reg_to_type(reg, t2, state);
      printf("push %s\n", reg);
      free_reg(reg, state);

      free_type(t2);
    }

    for (i = num_args; i > 0; --i) {
      if (i == 1) printf("pop %rdi\n");
      if (i == 2) printf("pop %rsi\n");
      if (i == 3) printf("pop %rdx\n");
      if (i == 4) printf("pop %rcx\n");
    }

    printf("push %r8\n");
    printf("push %r9\n");
    printf("push %r10\n");
    printf("push %r11\n");

    printf("mov %rsp, %r15\n");
    printf("mov $0xFFFFFFFFFFFFFFF0, %rax\n");
    printf("and %rax, %rsp\n");

    // number of floating point args in va list
    printf("mov $0, %rax\n");

    printf("call %s\n", func);

    printf("mov %r15, %rsp\n");

    printf("pop %r11\n");
    printf("pop %r10\n");
    printf("pop %r9\n");
    printf("pop %r8\n");

    ret = alloc_reg(state);
    printf("mov %rax, %s\n", ret);

    return ret;
  }

  assert(0);
}

char *compile_expr(struct ast_t *ast, struct cc_state_t *state, struct type_t *t) {
  struct type_t *t2 = t ? t : alloc_type();
  char *reg = 0;

  reg = compile_expr_(ast, state, t2);
  if (is_reg(reg))
    clamp_reg_to_type(reg, t2, state);

  if (t == 0)
    free_type(t2);

  return reg;
}

static int get_num_params(struct ast_t *ast) {
  int num_params;
  struct ast_t *decltor = ast->as_func_def.decltor;
  for (num_params = 0; decltor->as_decltor.params[num_params]; ++num_params);
  return num_params;
}

void compile(struct ast_t *ast, struct cc_state_t *state) {
  assert(ast);

  /** function definition */
  if (ast->type == func_def_ast) {
    struct ast_t *decltor = ast->as_func_def.decltor;
    struct cc_state_t *state_cp = 0;
    int i;
    int num_params = 0;

    struct type_t *t = alloc_type();
    copy_string(&t->type, ast->as_func_def.type->as_type_spec.name);
    t->indirection = get_decltor_indirection(decltor);

    /** add symbol to global scope */
    define_symbol(
      get_decltor_id(decltor),
      get_decltor_id(decltor),
      t,
      state);

    state_cp = init_subscope(state);

    state_cp->ret_lbl = ++labeli;

    printf(".text\n");
    printf(".globl %s\n", get_decltor_id(ast->as_func_def.decltor));
    printf("%s:\n", get_decltor_id(ast->as_func_def.decltor));

    /** save regs */
    printf("push %r11\n");
    printf("push %r12\n");
    printf("push %r13\n");
    printf("push %r14\n");
    printf("push %r15\n");

    /** pass function params in registers */
    if (decltor->as_decltor.params) {
      num_params = get_num_params(ast);

      for (i = num_params - 1; i >= 0; --i) {
        char *s;
        char *id;
        struct ast_t *fp = 0;
        struct type_t *t = 0;
        assert(i < 4);

        fp = decltor->as_decltor.params[i];

        if (i == 0) printf("push %rdi\n");
        if (i == 1) printf("push %rsi\n");
        if (i == 2) printf("push %rdx\n");
        if (i == 3) printf("push %rcx\n");

        id = get_decltor_id(fp->as_func_param.decltor);
        s = offset_reg((i + 1)*8, "%rbp");
        t = alloc_type();
        copy_string(&t->type, fp->as_func_param.type->as_type_spec.name);

        t->indirection = get_decltor_indirection(fp->as_func_param.decltor);

        define_symbol(id, s, t, state_cp);

        free_type(t);
        free(s);
      }
    }

    printf("push %rbp\n");
    printf("mov %rsp, %rbp\n");

    /** function body */
    //--t->indirection;
    state_cp->ret_type = t;
    compile(ast->as_func_def.body, state_cp);

    printf("LABEL%i:\n", state_cp->ret_lbl);
    printf("pop %rbp\n");

    /** remove args from stack */
    printf("add $%i, %rsp\n", num_params*8);

    /** restore saved regs */
    printf("pop %r15\n");
    printf("pop %r14\n");
    printf("pop %r13\n");
    printf("pop %r12\n");
    printf("pop %r11\n");

    printf("ret\n");

    cc_state_free(state_cp);

    printf(".data\n");
    while (state->str_def_i < state->stri) {
      printf("STR%i:\n.string ", state->str_def_i);
      printf("\"%s\"\n", state->str_lits[state->str_def_i]);
      ++state->str_def_i;
    }

    free_type(t);
    t = 0;

  /** declaration */
  } else if (ast->type == decl_ast) {
    if (state->prev == 0) {
      struct type_t *t = 0;

      printf(".data\n");

      define_struct(ast->as_decl.type, state);

      if (ast->as_decl.decltor) {
        t = alloc_type();
        copy_string(&t->type, ast->as_decl.type->as_type_spec.name);
        t->indirection = get_decltor_indirection(ast->as_decl.decltor);

        define_symbol(
          get_decltor_id(ast->as_decl.decltor),
          get_decltor_id(ast->as_decl.decltor),
          t, state);

        free_type(t);
      }

    } else {
      char *s;
      struct type_t *t;
      int sz;

      t = alloc_type();
      copy_string(&t->type, ast->as_decl.type->as_type_spec.name);
      t->indirection = get_decltor_indirection(ast->as_decl.decltor);

      sz = sizeof_type(t, state);
      state->stack_sz += sz;
      state->next_rbp -= sz;

      printf("sub $%i, %rsp\n", sz);

      s = offset_reg(state->next_rbp, "%rbp");

      if (ast->as_decl.init) {
        struct type_t *t2 = alloc_type();
        char *reg = compile_expr(ast->as_decl.init, state, t2);
        char *suf = "";

        if (sizeof_type(t, state) == 1)
          suf = "b";
        else if (sizeof_type(t, state) == 4)
          suf = "w";

        reg = ensure_reg(reg, state, t2);
        printf("mov%s %s%s, %s\n", suf, reg, suf, s);
        free_reg(reg, state);
        free_type(t2);
      }

      define_symbol(get_decltor_id(ast->as_decl.decltor), s, t, state);

      free_type(t);
      free(s);
    }

  /** statements */
  } else if (ast->type == cmpnd_stmt_ast) {
    int i;
    struct cc_state_t *scope = init_subscope(state);

    for (i = 0; ast->as_cmpnd_stmt.decls[i]; ++i)
      compile(ast->as_cmpnd_stmt.decls[i], scope);

    for (i = 0; ast->as_cmpnd_stmt.stmts[i]; ++i)
      compile(ast->as_cmpnd_stmt.stmts[i], scope);

    printf("add $%i, %rsp\n", scope->stack_sz);

    cc_state_free(scope);

  /** return statement */
  } else if (ast->type == return_stmt_ast) {
    if (ast->as_return_stmt.value) {
      char *reg = compile_expr(ast->as_return_stmt.value, state, 0);
      printf("mov %s, %rax\n", reg);
      free_reg(reg, state);
      clamp_reg_to_type("%rax", get_ret_type(state), state);
    }

    printf("add $%i, %rsp\n", get_ret_pop_sz(state));
    printf("jmp LABEL%i\n", state->ret_lbl);

  /** expression statement */
  } else if (ast->type == expr_stmt_ast) {
    char *reg = compile_expr(ast->as_expr_stmt.expr, state, 0);
    free_reg(reg, state);

  /** if statement */
  } else if (ast->type == if_stmt_ast) {
    char *reg = compile_expr(ast->as_if_stmt.cond, state, 0);
    int end_lbl = ++labeli;
    int else_lbl = ++labeli;

    printf("cmp $0, %s\n", reg);
    free_reg(reg, state);

    printf("je LABEL%i\n", else_lbl);
    compile(ast->as_if_stmt.body, state);
    printf("jmp LABEL%i\n", end_lbl);
    printf("LABEL%i:\n", else_lbl);

    if (ast->as_if_stmt.else_body)
      compile(ast->as_if_stmt.else_body, state);

    printf("LABEL%i:\n", end_lbl);

  /** while statement */
  } else if (ast->type == while_stmt_ast) {
    int start_lbl = ++labeli;
    int break_lbl = ++labeli;
    char *reg = 0;

    printf("LABEL%i:\n", start_lbl);
    reg = compile_expr(ast->as_while_stmt.cond, state, 0);
    printf("cmp $0, %s\n", reg);
    free_reg(reg, state);

    printf("je LABEL%i\n", break_lbl);

    compile(ast->as_while_stmt.body, state);

    printf("jmp LABEL%i\n", start_lbl);
    printf("LABEL%i:\n", break_lbl);

  /** for statement */
  } else if (ast->type == for_stmt_ast) {
    int start_lbl = ++labeli;
    int end_lbl = ++labeli;
    char *reg;

    reg = compile_expr(ast->as_for_stmt.init, state, 0);
    free_reg(reg, state);

    printf("LABEL%i:\n", start_lbl);

    reg = compile_expr(ast->as_for_stmt.cond, state, 0);
    printf("cmp $0, %s\n", reg);
    printf("je LABEL%i\n", end_lbl);
    free_reg(reg, state);

    compile(ast->as_for_stmt.body, state);

    reg = compile_expr(ast->as_for_stmt.update, state, 0);
    free_reg(reg, state);

    printf("jmp LABEL%i\n", start_lbl);
    printf("LABEL%i:\n", end_lbl);

  } else {
    printf("# unimplemeneted\n");
  }

  /** check for register leaks */
  if (state->regs) {
    fprintf(stderr, "register leak\n");
    state->regs = 0;
    assert(0);
  }
}

void cc_state_free(struct cc_state_t *state) {
  int i;
  if (state == 0) return;

  for (i = 0; i < state->sym_i; ++i) {
    free(state->sym_names[i]);
    free(state->sym_locs[i]);
    if (state->sym_types[i].type)
      free(state->sym_types[i].type);
  }

  for (i = 0; i < state->num_structs; ++i) {
    int j;
    struct struct_def_t *sd = &state->struct_defs[i];
    for (j = 0; j < sd->num_fields; ++j) {
      free(sd->types[j].type);
      free(sd->fields[j]);
    }
    free(state->struct_ids[i]);
  }

  if (state->prev == 0) {
    for (i = 0; i < state->stri; ++i) {
      free (state->str_lits[i]);
    }
  }

  free(state);
}
