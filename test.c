#ifdef DRBCC
int printf();
void *malloc(int sz);
void free(void *p);
void exit(int i);
#else
#include <stdio.h>
#include <stdlib.h>
#endif

void test(int i, char *msg) {
  if (i) return;
  printf("\033[31;1;4mtest '%s' failed\n\033[0m", msg);
  exit(1);
}

/** recursive fibonacci algorithm */
int recurfib(int n) {
  return n < 2 ? n : recurfib(n - 1) + recurfib(n - 2);
}

/** iterative fibonacci algorithm */
int iterfib(int n) {
  int i = 0;
  int j = 1;

  if (n < 2)
    return n;

  for (0; n > 1; --n) {
    int t;
    t = j;
    j = i + j;
    i = t;
  }

  return j;
}

int gcd(int a, int b) {
  return b == 0 ? a : gcd(b, a % b);
}

void test_functions() {
  test(recurfib(0) == 0, "fib 1");
  test(iterfib(0) == 0, "fib 2");

  test(recurfib(1) == 1, "fib 2");
  test(iterfib(1) == 1, "fib 3");

  test(recurfib(20) == 6765, "fib 4");
  test(iterfib(20) == 6765, "fib 5");

  test(gcd(1, 1) == 1, "gcd 1");
  test(gcd(12, 9) == 3, "gcd 2");
  test(gcd(22, 9) == 1, "gcd 2");
  test(gcd(1000, 750) == 250, "gcd 2");
}

void swap(int *i, int *j) {
  int t;
  t = *i;
  *i = *j;
  *j = t;
}

/** pointers tests */
void test_ptrs(int n, int *np) {
  int i = 3;
  int *ip = &i;
  int a;
  int b;

  test(*ip == i, "pointer 1");
  test(ip == &i, "pointer 2");
  test(&*&i == &i, "pointer 3");

  test(n == *np, "pointer 4");

  a = 0;
  b = 1;
  swap(&a, &b);
  test(a == 1, "pointer 5");
  test(b == 0, "pointer 6");
}

void test_logical_or() {
  int i;

  test(!(0 || 0), "logical or 1");
  test(1 || 0, "logical or 2");
  test(0 || 1, "logical or 3");
  test(1 || 1, "logical or 4");

  /** logical or should short circuit */
  i = 0;
  test((0 || (i = 1)) && i == 1, "logical or 5");
  i = 0;
  test((1 || (i = 1)) && i == 0, "logical or 6");
}

void test_logical_and() {
  int i;

  test(!(0 && 0), "logical and 1");
  test(!(1 && 0), "logical and 2");
  test(!(0 && 1), "logical and 3");
  test(1 && 1, "logical and 4");

  /** logical or should short circuit */
  i = 0;
  test((1 && (i = 1)) && i == 1, "logical and 5");
  i = 0;
  test(!(0 && (i = 1)) && i == 0, "logical and 6");
}

struct mystruct {
  int i;
  char c;

  struct {
    int j;
  } inner;

  union {
    int i;
    int j;
  } u;
};

void test_structs() {
  struct mystruct ms;
  struct mystruct *msp = &ms;
  ms.i = 123;

  test(ms.i == 123, "structs 1");
  test(ms.i == msp->i, "structs 2");

  ms.c = 'a';
  test(ms.c == 'a', "structs 3");
  test(ms.i == 123, "structs 4");

  // structs can have structs as members
  ms.inner.j = 3;
  test(ms.inner.j == 3, "structs 5");

  // union members should have offset of 0
  ms.u.i = 0;
  ms.u.j = 5;
  test(ms.u.i == 5, "unions 1");
}

int main(int argc, char **argv) {
  int n = 3;
  printf("Hello world\n");

  // run tests
  test_functions();
  test_ptrs(n, &n);
  test_logical_or();
  test_logical_and();
  test_structs();

  printf("\033[32;1;4mall tests passed\n\033[0m");

  return 0;
}
