#ifndef PARSE_H_INCLUDED
#define PARSE_H_INCLUDED

#include <stdlib.h>
#include <stdio.h>
#include <assert.h>
#include <string.h>

#include "drbcc.h"

static void *mem_buf = 0;
static long mem_sz = 0;

#define MAX_MEM (1024*1024)

static void *ast_alloc_mem(long sz) {
  if (mem_buf == 0)
    mem_buf = malloc(MAX_MEM);

  assert(mem_sz + sz < MAX_MEM);

  void *r = mem_buf + mem_sz;
  mem_sz += sz;
  return r;
}

struct ast_t *alloc_ast() {
  struct ast_t *p = (struct ast_t *)ast_alloc_mem(sizeof(struct ast_t));
  memset(p, 0, sizeof(struct ast_t));
  return p;
}

void free_ast_mem() {
  if (mem_buf) free(mem_buf);
  mem_buf = 0;
  mem_sz = 0;
}

/** holds the last token */
static char *last_tok_buf;

/** holds the current token */
static char *cur_tok_buf;

/** -1 means unintialized, 0 means EOF */
static int next_tok = -1;

int parse_done() {
  return next_tok == 0;
}

/** if tok matches the next token return true and consume it */
static int accept(int tok) {
  // first token
  if (next_tok == -1)
    next_tok = lex(&cur_tok_buf);

  if (next_tok == tok) {
    if (last_tok_buf == 0) {
      last_tok_buf = malloc(1);
      last_tok_buf[0] = 0;
    }

    last_tok_buf = realloc(last_tok_buf, 1 + strlen(cur_tok_buf));

    strcpy(last_tok_buf, cur_tok_buf);
    next_tok = lex(&cur_tok_buf);
    return 1;
  }

  return 0;
}

static void put_last_tok(char **str) {
  assert(*str == 0);

  *str = ast_alloc_mem(1 + strlen(last_tok_buf));
  strcpy(*str, last_tok_buf);
}

static void syntax_error() {
  fprintf(stderr, "syntax error on %s\n", cur_tok_buf);
  exit(-1);
}

/** check tok matches and error if it doesn't */
static void expect(int tok) {
  if (!accept(tok)) {
    fprintf(stderr, "syntax error: expected %i, got %s\n", tok, cur_tok_buf);
    exit(-1);
  }
}

/** helpers for parsing lists of things */
void ast_list_init(struct ast_t **list, int *sz) {
  list[0] = 0;
  *sz = 0;
}

struct ast_t *ast_list_push(struct ast_t **list, int *sz, int max_sz, struct ast_t *ast) {
  assert(*sz + 1 < max_sz);

  list[*sz] = ast;
  list[*sz + 1] = 0;
  (*sz)++;

  assert(list[*sz] == 0);
  return list[*sz - 1];
}

void ast_list_put(struct ast_t*** dst, struct ast_t **src, int *sz) {
  assert(*dst == 0);

  if (*sz == 0) {
    *dst = ast_alloc_mem(sizeof(struct ast_t *));
    *dst[0] = 0;
    return;
  }

  assert(src[*sz] == 0);

  *dst = ast_alloc_mem((*sz + 1)*sizeof(struct ast_t *));
  memcpy(*dst, src, (*sz + 1)*sizeof(struct ast_t *));

  src[0] = 0;
  *sz = 0;
}

/** rule definitions */

static struct ast_t *p_decltor();
static struct ast_t *p_decl();
static struct ast_t *p_expr();
static struct ast_t *p_assign_expr();
static struct ast_t *p_stmt();

static struct ast_t *p_type_spec() {
  static int struct_i = 0;
  struct ast_t *ast = alloc_ast();
  ast->type = type_spec_ast;

  if (accept(type_tok)) {
    put_last_tok(&ast->as_type_spec.name);

  } else if (next_tok == struct_tok || next_tok == union_tok) {
    int is_union = accept(union_tok);
    if (!is_union)
      expect(struct_tok);

    ast->as_type_spec.is_union = is_union;

    if (accept(id_tok)) {
      ast->as_type_spec.name = ast_alloc_mem(1 + strlen("struct") + 1 + strlen(last_tok_buf));
      sprintf(ast->as_type_spec.name, "struct %s", last_tok_buf);

    } else {
      ast->as_type_spec.name = ast_alloc_mem(1 + snprintf(0, 0, "__anon_struct%i", ++struct_i));
      sprintf(ast->as_type_spec.name, "__anon_struct%i", struct_i);
    }

    if (accept('{')) {
      int i;
      struct ast_t *decls[100];
      ast_list_init(decls, &i);

      while (!accept('}')) {
        struct ast_t *d = p_decl();
        ast_list_push(decls, &i, sizeof(decls), d);
      }

      ast_list_put(&ast->as_type_spec.decls, decls, &i);
    }

  } else {
    syntax_error();
  }

  return ast;
}

static struct ast_t *p_func_param() {
  struct ast_t *param = alloc_ast();
  param->type = func_param_ast;

  param->as_func_param.type = p_type_spec();
  param->as_func_param.decltor = p_decltor();

  return param;
}

static struct ast_t *p_const() {
  struct ast_t *ast = alloc_ast();

  if (accept(const_tok)) {
    ast->type = int_lit_ast;
    ast->as_int_lit.value = atoi(last_tok_buf);

  } else if (accept(char_lit_tok)) {
    ast->type = char_lit_ast;
    assert(last_tok_buf[0] == '\'');
    assert(last_tok_buf[2] == '\'');
    ast->as_char_lit.value = last_tok_buf[1];

  } else if (accept(str_lit_tok)) {
    ast->type = str_lit_ast;

    ast->as_str_lit.value = ast_alloc_mem(1 + strlen(last_tok_buf) - 2);
    strcpy(ast->as_str_lit.value, last_tok_buf + 1);
    ast->as_str_lit.value[strlen(last_tok_buf) - 2] = 0;

  } else {
    syntax_error();
  }

  return ast;
}

static struct ast_t *p_decltor() {
  struct ast_t *ast = alloc_ast();

  ast->type = decltor_ast;

  while (accept('*'))
    ++ast->as_decltor.ptr_lvl;

  if (accept(id_tok)) {
    ast->as_decltor.direct = alloc_ast();
    ast->as_decltor.direct->type = id_decltor_ast;
    put_last_tok(&ast->as_decltor.direct->as_id_decltor.id);

  } else if (accept('(')) {
    ast->as_decltor.direct = p_decltor();
    expect(')');

  } else {
    syntax_error();
  }

  while (next_tok == '[' || next_tok == '(') {
    /** function params */
    if (accept('(')) {
      int i = 0;
      struct ast_t *param_buf[100];
      ast_list_init(param_buf, &i);

      if (ast->as_decltor.params)
        syntax_error();

      if (next_tok == type_tok || next_tok == struct_tok) {
        ast_list_push(param_buf, &i, sizeof(param_buf), p_func_param());

        while (next_tok != ')') {
          expect(',');
          ast_list_push(param_buf, &i, sizeof(param_buf), p_func_param());
        }

        ast_list_put(&ast->as_decltor.params, param_buf, &i);
      }

      expect(')');

    /** array */
    } else if (accept('[')) {
      accept(const_tok);
      expect(']');
    }
  }

  return ast;
}

static struct ast_t *p_primary_expr() {
  if (accept('(')) {
    struct ast_t *ast = p_expr();
    expect(')');
    return ast;

  } else if (accept(id_tok)) {
    struct ast_t *ast = alloc_ast();
    ast->type = id_expr_ast;
    put_last_tok(&ast->as_id_expr.id);
    return ast;

  } else {
    return p_const();
  }

  assert(0);
}

static struct ast_t *p_postfix_expr() {
  struct ast_t *head = p_primary_expr();

  while (1) {
    if (accept(inc_tok)) {

    } else if (accept(dec_tok)) {

    } else if (accept(arrow_tok) || accept('.')) {
      expect(id_tok);

      struct ast_t *a = alloc_ast();
      a->type = arrow_expr_ast;
      put_last_tok(&a->as_arrow_expr.id);

      a->as_arrow_expr.lhs = head;
      head = a;

    } else if (accept('(')) {
      struct ast_t *ast = alloc_ast();

      struct ast_t *p_buf[100];
      int i = 0;
      ast_list_init(p_buf, &i);

      ast->type = func_call_expr_ast;
      ast->as_func_call_expr.func = head;

      if (!accept(')')) {
        ast_list_push(p_buf, &i, sizeof(p_buf), p_assign_expr());

        while (!accept(')')) {
          expect(',');
          ast_list_push(p_buf, &i, sizeof(p_buf), p_assign_expr());
        }
      }

      ast_list_put(&ast->as_func_call_expr.args, p_buf, &i);
      head = ast;

    } else {
      break;
    }
  }

  return head;
}

#define IS_UNARY_OP(t) (t == '&' || t == '*' || t == '+' || t == '-' || t == '~' || t == '!' || t == inc_tok || t == dec_tok)

static struct ast_t *p_unary_expr() {
  int ops[100];
  int i = 0;
  ops[0] = 0;

  if (IS_UNARY_OP(next_tok)) {
    struct ast_t *ast = alloc_ast();
    ast->type = unary_expr_ast;

    while (IS_UNARY_OP(next_tok)) {
      assert((i + 1) < sizeof(ops));
      ops[i] = next_tok;
      ops[++i] = 0;

      expect(next_tok);
    }

    ast->as_unary_expr.ops = ast_alloc_mem((i + 1)*sizeof(int));
    memcpy(ast->as_unary_expr.ops, ops, (i + 1)*sizeof(int));
    ast->as_unary_expr.rhs = p_postfix_expr();
    return ast;

  } else {
    return p_postfix_expr();
  }
}

#define DEFINE_INFIX_EXPR(name, next, test) \
static struct ast_t *name() { \
  struct ast_t *head = next(); \
\
  if (test) { \
    struct ast_t *terms[100]; \
    int i = 0; \
    ast_list_init(terms, &i); \
\
    struct ast_t *ast = alloc_ast(); \
    ast->type = infix_expr_ast; \
    ast->as_infix_expr.lhs = head; \
\
    while (test) { \
      struct ast_t *term = alloc_ast(); \
      term->type = infix_term_ast; \
      term->as_infix_term.op_tok = next_tok; \
\
      expect(next_tok); \
\
      term->as_infix_term.value = next(); \
\
      ast_list_push(terms, &i, sizeof(terms), term); \
    } \
\
    ast_list_put(&ast->as_infix_expr.terms, terms, &i); \
    return ast; \
\
  } else { \
    return head; \
  } \
}

DEFINE_INFIX_EXPR(p_mul_expr, p_unary_expr, (next_tok == '*' || next_tok == '/' || next_tok == '%'));
DEFINE_INFIX_EXPR(p_add_expr, p_mul_expr, (next_tok == '+' || next_tok == '-'));
DEFINE_INFIX_EXPR(p_shift_expr, p_add_expr, (next_tok == shl_tok || next_tok == shr_tok));
DEFINE_INFIX_EXPR(p_rel_expr, p_shift_expr, (next_tok == '<' || next_tok == '>'));
DEFINE_INFIX_EXPR(p_eq_expr, p_rel_expr, (next_tok == eq_tok || next_tok == neq_tok));
DEFINE_INFIX_EXPR(p_log_and_expr, p_eq_expr, (next_tok == log_and_tok));
DEFINE_INFIX_EXPR(p_log_or_expr, p_log_and_expr, (next_tok == log_or_tok));

static struct ast_t *p_tern_expr(int *proper) {
  struct ast_t *ast = p_log_or_expr();
  *proper = 0;

  if (accept('?')) {
    struct ast_t *tern = alloc_ast();
    tern->type = tern_expr_ast;
    tern->as_tern_expr.cond = ast;

    tern->as_tern_expr.true_expr = p_log_or_expr();
    expect(':');
    tern->as_tern_expr.false_expr = p_log_or_expr();
    *proper = 1;

    return tern;
  }

  return ast;
}

static struct ast_t *p_assign_expr() {
  int ternary;
  struct ast_t *ast = p_tern_expr(&ternary);

  if (ternary)
    return ast;

  if (accept('=')) {
    struct ast_t *assgn_ast = alloc_ast();
    assgn_ast->type = assign_expr_ast;
    assgn_ast->as_assign_expr.lhs = ast;
    assgn_ast->as_assign_expr.rhs = p_assign_expr();
    return assgn_ast;
  }

  return ast;
}

static struct ast_t *p_expr() {
  struct ast_t *ast = p_assign_expr();

  // TODO:
  while (accept(',')) {
    ast = p_assign_expr();
  }

  return ast;
}

static struct ast_t *p_initializer() {
  return p_assign_expr();
}

static struct ast_t *p_decl() {
  struct ast_t *ast = alloc_ast();

  ast->type = decl_ast;

  ast->as_decl.type = p_type_spec();

  if (accept(';')) {
    return ast;
  }

  ast->as_decl.decltor = p_decltor();

  if (!accept(';')) {
    expect('=');
    ast->as_decl.init = p_initializer();
    expect(';');
  }

  return ast;
}

/** statements */
static struct ast_t *p_cmpnd_stmt() {
  int i = 0;
  struct ast_t *ast_list[128] = {0};
  struct ast_t *ast = alloc_ast();
  ast_list_init(ast_list, &i);

  ast->type = cmpnd_stmt_ast;

  expect('{');

  while (next_tok == type_tok || next_tok == struct_tok)
    ast_list_push(ast_list, &i, sizeof(ast_list), p_decl());

  ast_list_put(&ast->as_cmpnd_stmt.decls, ast_list, &i);

  while (!accept('}'))
    ast_list_push(ast_list, &i, sizeof(ast_list), p_stmt());

  ast_list_put(&ast->as_cmpnd_stmt.stmts, ast_list, &i);

  return ast;
}

static struct ast_t *p_if_stmt() {
  struct ast_t *ast = alloc_ast();
  ast->type = if_stmt_ast;

  expect(if_tok);
  expect('(');
  ast->as_if_stmt.cond = p_expr();
  expect(')');

  ast->as_if_stmt.body = p_stmt();

  if (accept(else_tok)) {
    ast->as_if_stmt.else_body = p_stmt();
  }

  return ast;
}

static struct ast_t *p_for_stmt() {
  struct ast_t *ast = alloc_ast();
  ast->type = for_stmt_ast;

  expect(for_tok);

  expect('(');
  ast->as_for_stmt.init = p_expr();
  expect(';');
  ast->as_for_stmt.cond = p_expr();
  expect(';');
  ast->as_for_stmt.update = p_expr();
  expect(')');

  ast->as_for_stmt.body = p_stmt();

  return ast;
}

static struct ast_t *p_while_stmt() {
  struct ast_t *ast = alloc_ast();
  ast->type = while_stmt_ast;

  expect(while_tok);
  expect('(');
  ast->as_while_stmt.cond = p_expr();
  expect(')');

  ast->as_while_stmt.body = p_stmt();

  return ast;
}

static struct ast_t *p_stmt() {
  struct ast_t *ast = alloc_ast();

  ast->type = empty_stmt_ast;

  if (accept(';'))
    return ast;

  if (next_tok == '{') {
    ast = p_cmpnd_stmt();

  } else if (accept(return_tok)) {
    ast->type = return_stmt_ast;

    if (!accept(';')) {
      ast->as_return_stmt.value = p_expr();
      expect(';');
    }

    return ast;

  } else if (accept(break_tok)) {
    expect(';');

  } else if (next_tok == if_tok) {
    ast = p_if_stmt();

  } else if (next_tok == while_tok) {
    ast = p_while_stmt();

  } else if (next_tok == for_tok) {
    ast = p_for_stmt();

  } else {
    ast->type = expr_stmt_ast;
    ast->as_expr_stmt.expr = p_expr();
    expect(';');
  }

  return ast;
}

/** external declarations */
struct ast_t *p_ext_decl() {
  struct ast_t *ast = alloc_ast();
  struct ast_t *d = 0;
  struct ast_t *type = 0;

  type = p_type_spec();

  if (accept(';')) {
    ast->type = decl_ast;
    ast->as_decl.type = type;
    return ast;
  }

  d = p_decltor();

  if (accept('=')) {
    ast->as_decl.init = p_initializer();
    expect(';');

    ast->type = decl_ast;
    ast->as_decl.decltor = d;
    ast->as_decl.type = type;

  } else if (next_tok == '{') {
    ast->type = func_def_ast;
    ast->as_func_def.decltor = d;
    ast->as_func_def.body = p_cmpnd_stmt();
    ast->as_func_def.type = type;

  } else {
    expect(';');

    ast->type = decl_ast;
    ast->as_decl.decltor = d;
    ast->as_decl.type = type;
  }

  return ast;
}

struct ast_t *parse_extern_decl() {
  return p_ext_decl();
}

#endif
